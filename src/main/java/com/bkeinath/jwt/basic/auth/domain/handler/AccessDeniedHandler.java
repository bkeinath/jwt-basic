package com.bkeinath.jwt.basic.auth.domain.handler;

import java.nio.file.AccessDeniedException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class AccessDeniedHandler implements ExceptionMapper<AccessDeniedException> {

	private static final org.apache.logging.log4j.Logger log = org.apache.logging.log4j.LogManager.getLogger(AccessDeniedHandler.class);
	
	public Response toResponse(AccessDeniedException exn) {
		log.info(exn.getMessage());
        return Response.status(403).type("text/plain").entity(exn.getMessage()).build();
    }
}
