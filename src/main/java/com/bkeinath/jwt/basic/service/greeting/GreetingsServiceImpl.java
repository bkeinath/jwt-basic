package com.bkeinath.jwt.basic.service.greeting;

import javax.inject.Inject;

import com.bkeinath.jwt.basic.service.calculation.CalculationService;

public class GreetingsServiceImpl implements GreetingsService {

	private static final long serialVersionUID = 4872952424608601901L;
	
	@Inject
	private CalculationService calcService;
	

	@Override
	public String getGreeting() {
		return "Hello! The calculation is: " + calcService.getValue();
	}

}
