package com.bkeinath.jwt.basic.test;

import java.security.Principal;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import com.bkeinath.jwt.basic.auth.domain.User;
import com.bkeinath.jwt.basic.service.greeting.GreetingsService;
import com.google.gson.Gson;

@Path("/test")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TestController {

	private static final org.apache.logging.log4j.Logger log = org.apache.logging.log4j.LogManager.getLogger(TestController.class);

	@Context
	private SecurityContext securityContext;
	
	@Inject
	private GreetingsService greetingsService;
	
	@PermitAll
	@GET
	@Path("test")
	public Response test() {
		log.info("Testing");
		log.info(greetingsService.getGreeting());
		printUserPrincipal(securityContext.getUserPrincipal());
		Gson gson = new Gson();
		return Response.ok(gson.toJson("test works")).build();
	}
	
	@RolesAllowed("TEST1")
	@GET
	@Path("test1")
	public Response test1() {
		log.info("Testing 1");
		log.info(greetingsService.getGreeting());
		printUserPrincipal(securityContext.getUserPrincipal());
		Gson gson = new Gson();
		return Response.ok(gson.toJson("test1 works")).build();
	}
	
	@RolesAllowed("TEST2")
	@GET
	@Path("test2")
	public Response test2() {
		log.info("Testing 2");
		log.info(greetingsService.getGreeting());
		printUserPrincipal(securityContext.getUserPrincipal());
		Gson gson = new Gson();
		return Response.ok(gson.toJson("test2 works")).build();
	}
	
	private void printUserPrincipal(Principal user) {
		if (user == null) {
			log.warn("User is null");
		} else {
			User u = (User) user;
			log.info("Username: {}", u.getName());
			log.info("Level: {}", u.getLevel());
			log.info("Roles: {}", u.getRoles());
		}
	}
}
