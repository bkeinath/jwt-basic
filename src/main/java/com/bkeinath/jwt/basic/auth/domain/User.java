package com.bkeinath.jwt.basic.auth.domain;

import java.io.Serializable;
import java.security.Principal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;

import com.bkeinath.utils.time.TimeUtil;

public class User implements Principal,Serializable {

	private static final long serialVersionUID = 1683735329358413531L;
	
	private String token;
	private String refId;
	private String name;
	private String newPassword; //will not show current password.
	private String recoveryEmail;
	private boolean smsEnabled;
	private String smsAddress;
	private int level;
	private DateTime paymentExpire;
	private DateTime lastLogin;
	private List<String> roles;
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getRefId() {
		return refId;
	}
	public void setRefId(String refId) {
		this.refId = refId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getRecoveryEmail() {
		return recoveryEmail;
	}
	public void setRecoveryEmail(String recoveryEmail) {
		this.recoveryEmail = recoveryEmail;
	}
	public boolean isSmsEnabled() {
		return smsEnabled;
	}
	public void setSmsEnabled(boolean smsEnabled) {
		this.smsEnabled = smsEnabled;
	}
	public String getSmsAddress() {
		return smsAddress;
	}
	public void setSmsAddress(String smsAddress) {
		this.smsAddress = smsAddress;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public DateTime getPaymentExpire() {
		return paymentExpire;
	}
	public void setPaymentExpire(DateTime paymentExpire) {
		this.paymentExpire = paymentExpire;
	}
	public DateTime getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(DateTime lastLogin) {
		this.lastLogin = lastLogin;
	}
	public String getLastLoginStr() {
		if (lastLogin != null) {
			return TimeUtil.getDateTimeDisplayString(lastLogin);
		}
		return "null";
	}
	public List<String> getRoles() {
		return roles == null ? Collections.emptyList() : roles;
	}
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
				.append(refId)
				.append(name)
				.append(recoveryEmail)
				.append(smsEnabled)
				.append(smsAddress)
				.append(level)
				.append(paymentExpire)
				.toString();
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				token
				,refId
				,name
				,recoveryEmail
				,smsEnabled
				,smsAddress
				,level
				);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !this.getClass().equals(obj.getClass())) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		
		final User other = (User) obj;
		return Objects.equals(this.token, other.token)
				&& Objects.equals(this.refId, other.refId)
				&& Objects.equals(this.name, other.name)
				&& Objects.equals(this.recoveryEmail, other.recoveryEmail)
				&& Objects.equals(this.smsEnabled, other.smsEnabled)
				&& Objects.equals(this.smsAddress, other.smsAddress)
				&& Objects.equals(this.level, other.level)
				;
	}
}
