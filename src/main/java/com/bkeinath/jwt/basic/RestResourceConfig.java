package com.bkeinath.jwt.basic;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;


@ApplicationPath("/api")
public class RestResourceConfig extends ResourceConfig {

	public RestResourceConfig() {
		packages("com.bkeinath.jwt.basic");
		register(RolesAllowedDynamicFeature.class); //required for ROLE checking in controllers
	}

}