package com.bkeinath.jwt.basic.auth.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bkeinath.jwt.basic.auth.domain.Role;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

public class JwtUtils implements Serializable {

	private static final long serialVersionUID = 2885703034422418911L;
	private static final org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(JwtUtils.class);

	private String jwtSecret = "bkeinathSecretKey";

	private int jwtExpirationMs = 86400000;

	public String generateJwtToken(String username) {
		Map<String, Object> claims = new HashMap<>();
		claims.put("sub", username);
		
		//TODO database lookup to get info based on username
		claims.put("level", 3);
		List<String> roles = new ArrayList<>();
		if (username.equals("user1") || username.equals("admin") ) {
			roles.add(Role.TEST1.toString());
		} 
		if (username.equals("user2") || username.equals("admin") ) {
			roles.add(Role.TEST2.toString());
		}
		
		claims.put("roles", roles);
		
		return Jwts.builder()
				.setClaims(claims)
				.setIssuedAt(new Date())
				.setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
				.signWith(SignatureAlgorithm.HS512, jwtSecret)
				.compact();
	}

	public String getUserNameFromJwtToken(String token) {
		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
	}
	
	public Integer getLevelFromJwtToken(String token) {
		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().get("level", Integer.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getRolesFromJwtToken(String token) {
		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().get("roles", List.class);
	}

	public boolean validateJwtToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException e) {
			logger.error("Invalid JWT signature: {}", e.getMessage());
		} catch (MalformedJwtException e) {
			logger.error("Invalid JWT token: {}", e.getMessage());
		} catch (ExpiredJwtException e) {
			logger.error("JWT token is expired: {}", e.getMessage());
		} catch (UnsupportedJwtException e) {
			logger.error("JWT token is unsupported: {}", e.getMessage());
		} catch (IllegalArgumentException e) {
			logger.error("JWT claims string is empty: {}", e.getMessage());
		}

		return false;
	}
}
