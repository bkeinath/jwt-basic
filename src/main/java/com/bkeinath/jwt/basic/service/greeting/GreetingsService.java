package com.bkeinath.jwt.basic.service.greeting;

import java.io.Serializable;

public interface GreetingsService extends Serializable {

	String getGreeting();
}
