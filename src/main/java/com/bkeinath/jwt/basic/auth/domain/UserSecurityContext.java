package com.bkeinath.jwt.basic.auth.domain;

import java.security.Principal;

import javax.ws.rs.core.SecurityContext;

public class UserSecurityContext implements SecurityContext {

	private static final org.apache.logging.log4j.Logger log = org.apache.logging.log4j.LogManager.getLogger(UserSecurityContext.class);
	
	private User user;
	
	public UserSecurityContext(User user) {
		this.user = user;
	}
	
	@Override
	public Principal getUserPrincipal() {
		return user;
	}

	@Override
	public boolean isUserInRole(String role) {
		boolean result = user.getRoles().stream().anyMatch(r -> r.equalsIgnoreCase(role));
		log.info("isUserInRole: {}", result);
		return result;
	}

	@Override
	public boolean isSecure() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getAuthenticationScheme() {
		// TODO Auto-generated method stub
		return null;
	}

}
