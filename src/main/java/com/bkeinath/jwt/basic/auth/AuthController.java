package com.bkeinath.jwt.basic.auth;

import java.nio.file.AccessDeniedException;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import com.bkeinath.jwt.basic.auth.domain.JwtResponse;
import com.bkeinath.jwt.basic.auth.domain.LoginRequest;
import com.bkeinath.jwt.basic.auth.utils.JwtUtils;
import com.bkeinath.jwt.basic.service.greeting.GreetingsService;
import com.google.gson.Gson;

@Path("/auth")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AuthController {

	private static final org.apache.logging.log4j.Logger log = org.apache.logging.log4j.LogManager.getLogger(AuthController.class);
	
	@Inject
	private GreetingsService greetingsService;
	
	@Inject
	private JwtUtils jwtUtils;
	
	@Context
	private SecurityContext securityContext;
	
	@GET
	@Path("test")
	public Response test() {
		log.info("Testing Auth");
		Gson gson = new Gson();
		return Response.ok(gson.toJson("auth works")).build();
	}
	
	@POST
	@Path("signin")
	public JwtResponse signin(@Valid LoginRequest loginRequest) {
		log.info("signing in");
		log.info(loginRequest);
		log.info(loginRequest.getUsername());
		log.info(loginRequest.getPassword());
		
		//TODO lookup account by username and verify password
		log.info(greetingsService.getGreeting());
		
		//if not correct, bad response/ not authorized
		//TODO throw 
		
		//else, sign in successful, generate token and return it
		String jwt = jwtUtils.generateJwtToken(loginRequest.getUsername());

		return new JwtResponse(jwt, loginRequest.getUsername());
	}

	@POST
	@Path("ex")
	public JwtResponse exception() throws AccessDeniedException {
		throw new AccessDeniedException("Invalid login!");
	}
}
