package com.bkeinath.jwt.basic.auth.filter;

import java.io.IOException;
import java.util.List;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;

import com.bkeinath.jwt.basic.auth.domain.User;
import com.bkeinath.jwt.basic.auth.domain.UserSecurityContext;
import com.bkeinath.jwt.basic.auth.utils.JwtUtils;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class JwtFilter implements ContainerRequestFilter {

	private static final org.apache.logging.log4j.Logger log = org.apache.logging.log4j.LogManager.getLogger(JwtFilter.class);
			
	@Inject
	private JwtUtils jwtUtils;
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		String jwt = parseJwt(requestContext);
		User user = null;
		if (jwt != null && jwtUtils.validateJwtToken(jwt)) {
			String username = jwtUtils.getUserNameFromJwtToken(jwt);
			Integer level = jwtUtils.getLevelFromJwtToken(jwt);
			List<String> roles = jwtUtils.getRolesFromJwtToken(jwt);
			log.info("Username: {} Level: {}", username,level);
			user = new User();
			user.setName(username);
			user.setLevel(level);
			user.setRoles(roles);
			requestContext.setSecurityContext(new UserSecurityContext(user));
		}

		
	}

	private String parseJwt(ContainerRequestContext requestContext) {
		String headerAuth = requestContext.getHeaderString("Authorization");

		if (StringUtils.isNotBlank(headerAuth) && headerAuth.startsWith("Bearer ")) {
			return headerAuth.substring(7, headerAuth.length());
		}

		return null;
	}
}
