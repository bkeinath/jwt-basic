package com.bkeinath.jwt.basic.service.calculation;

import java.io.Serializable;

public interface CalculationService extends Serializable {

	Integer getValue();
}
